FROM ubuntu:20.04

RUN apt-get update && apt-get install -y wget

RUN wget -O /usr/local/bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64
RUN chmod +x /usr/local/bin/hadolint

CMD ["hadolint","--version"]

